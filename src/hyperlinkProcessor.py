import requests
from datetime import datetime
from boto3.dynamodb.conditions import Attr

import boto3

session = boto3.session.Session(region_name='eu-central-1',
                            aws_access_key_id='AKIAJHK4UKVNUFHNWRVQ',
                            aws_secret_access_key='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'
)

class Hyperlink_Processor:
    def __init__(self,id,url,parent_url_id,parent_url,city_name):
        self.db=session.resource('dynamodb')
        self.s3=session.resource('s3')
        self.id= id
        self.parent_url_id= parent_url_id
        self.url=url
        self.parent_url=parent_url
        self.city_name=city_name

    def get_pdf(self):
        try:
            req = requests.get(self.url,stream =True)
            return req.content
        except Exception as e:
            return e

    def put_object (self,content):
        try:
            object= self.s3.Object('iw-bd-demowebcrawler-pdf',str(self.city_name)+'+'+str(self.parent_url_id)+'/'+str(self.id)+'.pdf')
            res= object.put(Body=content)
            print(res)
            self.mark_downloaded()
        except Exception as e:
            return e

    def mark_visited(self):
        self.db.Table('urlsList').update_item(
            Key={
                'parent_url': str(self.parent_url),
                'full_url': str(self.url)
            },
            UpdateExpression="SET visited=:var1, visited_on=:var2",
            ExpressionAttributeValues={
                ':var1': True,
                ':var2':str(datetime.utcnow().isoformat())
            }
        )

    def mark_downloaded(self):
        print(datetime.utcnow().isoformat())
        self.db.Table('urlsList').update_item(
            Key={
                'parent_url': self.parent_url,
                'full_url': self.url
            },
            UpdateExpression="SET downloaded=:var1, downloaded_on=:var2",
            ExpressionAttributeValues={
                ':var1': True,
                ':var2': str(datetime.utcnow().isoformat())
            },
        )

    def visit(self):
        self.mark_visited()
        content = self.get_pdf()
        self.put_object(content)

def main(event,context):
    print(str(datetime.utcnow().isoformat()))
    for record in event['Records']:
        url = str(record['body'])
        payload = record['messageAttributes']
        parent_id= payload['parent_URL_ID']['stringValue']
        parent_url = payload['parent_URL']['stringValue']
        city_name= payload['cityName']['stringValue']
        id= payload['ID']['stringValue']
        processor = Hyperlink_Processor(id,url,parent_id,parent_url,city_name)
        processor.visit()


if __name__== "__main__":
    event={'hello':'world'}
    main(event, {})